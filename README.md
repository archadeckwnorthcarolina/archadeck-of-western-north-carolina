At Archadeck of Western North Carolina, our local branch is part of a fantastic franchise that has created over 100,000 incredible outdoor structures across the county since 1980. We are the largest local builder of decks, outdoor kitchens, open and closed porches, patios and more. We serve most of the Western NC area; Buncombe, Cherokee, Haywood, Henderson, Macon, Madison, Mcdowell, Polk, Rutherford, Transylvania, Yancey and surrounding communities.

Website: https://western-nc.archadeck.com/
